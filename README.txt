What Is This?
-------------
This module generate blocks for show countries information.

This module works with mashape API https://www.mashape.com/ to retrieve information about countries around the world.

################
How To Install The Modules
#####################--------------------------
First you need to create your application in https://market.mashape.com/dashboard.

a) First download the mashape library https://github.com/Mashape/unirest-php
and copy files from "src" folder inside /sites/all/libraries/mashape/ folder, then you can see Unirest.php inside mashape folder /sites/all/libraries/mashape/Unirest.php.

b) You need to note these data:

1- X-Mashape-Key.
2- Codification for query (usually 'application/json' without quotes).

b) Then visit /admin/config/services/countriesapi in your drupal site and set the right values for X-Mashape-Key and Codification.

Note: First time when you installed the module, after click "Save Api" button, you need to press in "Update countries list" button to index data.

After you save your Mashape API KEY you can visit /admin/config/services/countriesapi/blocks-config and set number of country info blocks 
you want to create for your site , and after you can active your new blocks in "drupal block manager" (or use with another modules), at
this point you have Nº country info blocks, and one random country block (it show diferent country each time page reload). 

IMPORTANT: is necessary to visit new blocks at least once and press "save block" for each one admin/structure/block.



