<?php

/**
 * @file
 * Views 3 support for Location Phone.
 */

/**
 * Implements hook_views_data().
 */
function world_locations_views_data() {

    $data = array();

    // Specify the group our table will be part of.
    $data['country_list']['table']['group'] = t('Country list');

    // Need to tell views UI that table is a base table, this means we can create a View that uses this table to start from.
    $data['country_list']['table']['base'] = array(
      'title' => t('Country list'),
      'help' => t('Contains data about countries for expose to views.'),
    );

    /* ##########################################################################################
     *  tell it also how to handle the table columns for displaying, filtering and sorting
     */
    ##########################################################################################  */   
    // country_list -- fields.
    $data['country_list']['name'] = array(
      'title' => t('Name'),
      'help' => t('Country name.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_country_name',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['register_path'] = array(
      'title' => t('Register path'),
      'help' => t('Country register path.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_argument_string',
        'allow empty' => TRUE,
      ),
      'field' => array(
        'handler' => 'views_handler_field_url',
        'click sortable' => TRUE,
      ),
    );

    $data['country_list']['search_path'] = array(
      'title' => t('Search path'),
      'help' => t('Country search_path.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_argument_string',
        'allow empty' => TRUE,
      ),
      'field' => array(
        'handler' => 'views_handler_field_url',
        'click sortable' => TRUE,
      ),
    );

    $data['country_list']['capital'] = array(
      'title' => t('Capital'),
      'help' => t('Country capital.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['region'] = array(
      'title' => t('Country region'),
      'help' => t('Country region.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_country_region',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['subregion'] = array(
      'title' => t('Country subregion'),
      'help' => t('Country subregion.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['relevance'] = array(
      'title' => t('Country relevance'),
      'help' => t('Country relevance.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['demonym'] = array(
      'title' => t('Country demonym'),
      'help' => t('Country demonym.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['area'] = array(
      'title' => t('Country area'),
      'help' => t('Country area.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['gini'] = array(
      'title' => t('Country gini'),
      'help' => t('Country gini.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['nativeName'] = array(
      'title' => t('Country nativeName'),
      'help' => t('Country nativeName.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['alpha2Code'] = array(
      'title' => t('Country alpha2Code'),
      'help' => t('Country alpha2Code.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['alpha3Code'] = array(
      'title' => t('Country alpha3Code'),
      'help' => t('Country alpha3Code.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['timezones'] = array(
      'title' => t('Country timezones'),
      'help' => t('Country timezones.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['currencies'] = array(
      'title' => t('Country currencies'),
      'help' => t('Country currencies.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );


    $data['country_list']['translations'] = array(
      'title' => t('Country translations'),
      'help' => t('Country translations.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['languages'] = array(
      'title' => t('Country languages'),
      'help' => t('Country languages.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['callingCodes'] = array(
      'title' => t('Country callingCodes'),
      'help' => t('Country callingCodes.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['borders'] = array(
      'title' => t('Country borders'),
      'help' => t('Country borders.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['latlng'] = array(
      'title' => t('Country latlng'),
      'help' => t('Country latlng.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    $data['country_list']['altSpellings'] = array(
      'title' => t('Country altSpellings'),
      'help' => t('Country altSpellings.'),
      'field' => array(
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
        'empty field name' => t('None'),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
        'allow empty' => TRUE,
      ),
    );

    return $data;
}
