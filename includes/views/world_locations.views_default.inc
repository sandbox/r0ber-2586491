<?php

/**
 * @file
 * Views 3 default views.
 */

/**
 * Implements hook_views_data().
 */
function world_locations_views_default_views() {
    $view = new view();
    $view->name = 'country_list';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'country_list';
    $view->human_name = 'Country list';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Country list';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'subregion' => 'subregion',
      'capital' => 'capital',
      'region' => 'region',
      'subregion_1' => 'subregion_1',
      'name' => 'name',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
      'subregion' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'capital' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'region' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'subregion_1' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'name' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
    );
    /* Field: Country list: Name */
    $handler->display->display_options['fields']['name']['id'] = 'name';
    $handler->display->display_options['fields']['name']['table'] = 'country_list';
    $handler->display->display_options['fields']['name']['field'] = 'name';
    /* Field: Country list: Capital */
    $handler->display->display_options['fields']['capital']['id'] = 'capital';
    $handler->display->display_options['fields']['capital']['table'] = 'country_list';
    $handler->display->display_options['fields']['capital']['field'] = 'capital';
    /* Field: Country list: Country region */
    $handler->display->display_options['fields']['region']['id'] = 'region';
    $handler->display->display_options['fields']['region']['table'] = 'country_list';
    $handler->display->display_options['fields']['region']['field'] = 'region';
    $handler->display->display_options['fields']['region']['label'] = 'Region';
    /* Field: Country list: Country subregion */
    $handler->display->display_options['fields']['subregion_1']['id'] = 'subregion_1';
    $handler->display->display_options['fields']['subregion_1']['table'] = 'country_list';
    $handler->display->display_options['fields']['subregion_1']['field'] = 'subregion';
    $handler->display->display_options['fields']['subregion_1']['label'] = 'Subregion';
    /* Field: Country list: Country alpha2Code */
    $handler->display->display_options['fields']['alpha2Code']['id'] = 'alpha2Code';
    $handler->display->display_options['fields']['alpha2Code']['table'] = 'country_list';
    $handler->display->display_options['fields']['alpha2Code']['field'] = 'alpha2Code';
    $handler->display->display_options['fields']['alpha2Code']['label'] = 'Alpha2Code';
    /* Field: Country list: Country alpha3Code */
    $handler->display->display_options['fields']['alpha3Code']['id'] = 'alpha3Code';
    $handler->display->display_options['fields']['alpha3Code']['table'] = 'country_list';
    $handler->display->display_options['fields']['alpha3Code']['field'] = 'alpha3Code';
    $handler->display->display_options['fields']['alpha3Code']['label'] = 'Alpha3Code';
    /* Field: Country list: Country altSpellings */
    $handler->display->display_options['fields']['altSpellings']['id'] = 'altSpellings';
    $handler->display->display_options['fields']['altSpellings']['table'] = 'country_list';
    $handler->display->display_options['fields']['altSpellings']['field'] = 'altSpellings';
    $handler->display->display_options['fields']['altSpellings']['label'] = 'AltSpellings';
    /* Field: Country list: Country area */
    $handler->display->display_options['fields']['area']['id'] = 'area';
    $handler->display->display_options['fields']['area']['table'] = 'country_list';
    $handler->display->display_options['fields']['area']['field'] = 'area';
    $handler->display->display_options['fields']['area']['label'] = 'Area';
    /* Field: Country list: Country borders */
    $handler->display->display_options['fields']['borders']['id'] = 'borders';
    $handler->display->display_options['fields']['borders']['table'] = 'country_list';
    $handler->display->display_options['fields']['borders']['field'] = 'borders';
    $handler->display->display_options['fields']['borders']['label'] = 'Borders';
    /* Field: Country list: Country callingCodes */
    $handler->display->display_options['fields']['callingCodes']['id'] = 'callingCodes';
    $handler->display->display_options['fields']['callingCodes']['table'] = 'country_list';
    $handler->display->display_options['fields']['callingCodes']['field'] = 'callingCodes';
    $handler->display->display_options['fields']['callingCodes']['label'] = 'Calling Codes';
    /* Field: Country list: Country currencies */
    $handler->display->display_options['fields']['currencies']['id'] = 'currencies';
    $handler->display->display_options['fields']['currencies']['table'] = 'country_list';
    $handler->display->display_options['fields']['currencies']['field'] = 'currencies';
    $handler->display->display_options['fields']['currencies']['label'] = 'Currencies';
    /* Field: Country list: Country demonym */
    $handler->display->display_options['fields']['demonym']['id'] = 'demonym';
    $handler->display->display_options['fields']['demonym']['table'] = 'country_list';
    $handler->display->display_options['fields']['demonym']['field'] = 'demonym';
    $handler->display->display_options['fields']['demonym']['label'] = 'Demonym';
    /* Field: Country list: Country languages */
    $handler->display->display_options['fields']['languages']['id'] = 'languages';
    $handler->display->display_options['fields']['languages']['table'] = 'country_list';
    $handler->display->display_options['fields']['languages']['field'] = 'languages';
    $handler->display->display_options['fields']['languages']['label'] = 'Languages';
    /* Field: Country list: Country latlng */
    $handler->display->display_options['fields']['latlng']['id'] = 'latlng';
    $handler->display->display_options['fields']['latlng']['table'] = 'country_list';
    $handler->display->display_options['fields']['latlng']['field'] = 'latlng';
    $handler->display->display_options['fields']['latlng']['label'] = 'Latlng';
    /* Field: Country list: Country nativeName */
    $handler->display->display_options['fields']['nativeName']['id'] = 'nativeName';
    $handler->display->display_options['fields']['nativeName']['table'] = 'country_list';
    $handler->display->display_options['fields']['nativeName']['field'] = 'nativeName';
    $handler->display->display_options['fields']['nativeName']['label'] = 'NativeName';
    /* Field: Country list: Country relevance */
    $handler->display->display_options['fields']['relevance']['id'] = 'relevance';
    $handler->display->display_options['fields']['relevance']['table'] = 'country_list';
    $handler->display->display_options['fields']['relevance']['field'] = 'relevance';
    $handler->display->display_options['fields']['relevance']['label'] = 'Relevance';
    /* Field: Country list: Country timezones */
    $handler->display->display_options['fields']['timezones']['id'] = 'timezones';
    $handler->display->display_options['fields']['timezones']['table'] = 'country_list';
    $handler->display->display_options['fields']['timezones']['field'] = 'timezones';
    $handler->display->display_options['fields']['timezones']['label'] = 'Timezones';
    /* Field: Country list: Country translations */
    $handler->display->display_options['fields']['translations']['id'] = 'translations';
    $handler->display->display_options['fields']['translations']['table'] = 'country_list';
    $handler->display->display_options['fields']['translations']['field'] = 'translations';
    $handler->display->display_options['fields']['translations']['label'] = 'Translations';

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'country-list';

    /* Display: Block */
    $handler = $view->new_display('block', 'Block', 'block_1');

    // Add view to list of views to provide.
    $views[$view->name] = $view;


    // At the end, return array of default views.
    return $views;
}
