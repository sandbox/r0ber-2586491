<?php

/**
 * @file
 * Definition of views_handler_filter_vocabulary_machine_name.
 */

/**
 * Filter by vocabulary machine name.
 *
 * @ingroup views_filter_handlers
 */
module_load_include('inc', 'world_locations', 'world_locations.admin');

class views_handler_filter_country_name extends views_handler_filter_in_operator {
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();
    $countries = get_all_countries();
    //$vocabularies = taxonomy_get_vocabularies();
   /* foreach ($vocabularies as $voc) {
      $this->value_options[$voc->machine_name] = $voc->name;
    }*/
    foreach ($countries as $country) {
        if (!empty($country->name)) {
            $this->value_options[$country->name] = $country->name;
        }
    }
  }
}
