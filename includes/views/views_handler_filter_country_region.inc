<?php

/**
 * @file
 * Definition of views_handler_filter_vocabulary_machine_name.
 */
/**
 * Filter by vocabulary machine name.
 *
 * @ingroup views_filter_handlers
 */
module_load_include('inc', 'world_locations', 'world_locations.admin');

class views_handler_filter_country_region extends views_handler_filter_in_operator {
    function get_value_options() {
        if (isset($this->value_options)) {
            return;
        }

        $this->value_options = array();
        $countries = get_all_countries();
  
        foreach ($countries as $country) {
            if (!empty($country->region)) {
                $this->value_options[$country->region] = $country->region;
            }
        }
    }
}
