<?php

/**
 * @file
 * Admin configuration..
 */
/*
 * Api configuration params form.
 */
function world_locations_form_api_countries($form, &$form_state) {


    if (empty(variable_get('X-Mashape-Key', ''))) {
        drupal_set_message(t('Please add your X-Mashape-Key credentials'), 'warning');
    }
    // Consultamos el valor actual del campo "field_investigator_privacity"
    $countries = get_all_countries();


    // If not exists countries list yet, we query the API to retrieve country list.
    if (count($countries) < 1) {
        _update_countries_list(
            variable_get('X-Mashape-Key', 'application/json'), variable_get('Encode', '')
        );
    }

    $form['countries'] = array(
      '#type' => 'container',
      '#title' => t('Countries list'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ($countries as $country) {

        //If some countries haav not subregion.
        if (empty($country->region)) {
            $country->region = 'Unknown';
        }


        if (!$form['countries'][trim($country->region)]) {
            $form['countries'][$country->region] = array(
              '#type' => 'fieldset',
              '#title' => t($country->region),
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
              '#tree' => TRUE,
            );
        }


        // If subregion has at last one country.
        if (count($form['countries'][trim($country->region)]) > 0) {
            $form['countries'][trim($country->region)][$country->name] = array(
              '#type' => 'item',
              '#title' => t($country->name),
            );
        }
        else {
            $form['countries'][trim($country->region)]['empty'] = array(
              '#type' => 'markup',
              '#markup' => t('No countries added'),
            );
        }
    }

    $form['keyapp'] = array(
      '#type' => 'textfield', //you can find a list of available types in the form api
      '#title' => 'X-Mashape-Key?',
      '#size' => 60,
      '#maxlength' => 60,
      '#required' => TRUE, //make this field required 
      '#default_value' => variable_get('X-Mashape-Key', ''),
    );

    $form['encode'] = array(
      '#type' => 'textfield', //you can find a list of available types in the form api
      '#title' => 'Codification',
      '#size' => 60,
      '#maxlength' => 60,
      '#required' => TRUE, //make this field required 
      '#default_value' => variable_get('encode', 'application/json'),
    );

    $form['submit_button'] = array(
      '#type' => 'submit',
      '#value' => t('Save API'),
    );

    $form['update_button'] = array(
      '#type' => 'submit',
      '#value' => t('Update countries list'),
      '#submit' => array('_update_countries_list'),
    );

    $form['#submit'][] = 'update_api';
    //array_push($form['#submit'], 'update_api');
    // return system_settings_form($form);
    return $form;
}

function update_api($form, &$form_state) {
    variable_set('X-Mashape-Key', $form_state['values']['keyapp']);
    variable_set('encode', $form_state['values']['encode']);
    /*
      $update = db_update('countries_api') // Table name no longer needs {}
      ->fields(array('keyapp' => $form_state['values']['X-Mashape-Key']))
      ->fields(array('codification' => $form_state['values']['codification']))
      ->condition('id', 1, '=')
      ->execute();
     */
}

/**
 * Update or add countries to database (always delete before to make this task).
 * @param type $appKey
 * @param type $codification
 */
function _update_countries_list($form, &$form_state) {

    $app_key = variable_get('X-Mashape-Key', '');
    $codification = variable_get('encode', 'application/json');

    // Query all countries.
    $result = db_query('SELECT co.id
                        FROM {country_list} co '
    );

    $countries = $result->fetchAll();

    // Ensure we are deleteing all countries existing already before to insert new data.
    if (count($countries) > 0) {
        $num_deleted = db_delete('country_list')->execute();
    }

    // Update Conutries list.
    $library = libraries_detect($name);
    if(!$library){
      drupal_set_message(t('Please installa mashape library, please read readme.txt'), 'warning');
    }
    
    libraries_load('mashape');
  
    // These code snippets use an open-source library. http://unirest.io/php
    $response = Unirest\Request::get("https://restcountries-v1.p.mashape.com/all", array(
          "X-Mashape-Key" => trim($app_key),
          "Accept" => trim($codification)
            )
    );

    if (is_array($response->body)) {

        // Update each country to table.
        foreach ($response->body as $tab_index => $tab_value) {

            $timezones = ($response->body[$tab_index]->timezones) ? 
            implode(",", $response->body[$tab_index]->timezones) :
             $response->body[$tab_index]->timezones[0];

            $fields = array('name' => $response->body[$tab_index]->name,
              'capital' => $response->body[$tab_index]->capital,
              'region' => $response->body[$tab_index]->region,
              'subregion' => $response->body[$tab_index]->subregion,
              'relevance' => $response->body[$tab_index]->relevance,
              'population' => $response->body[$tab_index]->population,
              'demonym' => $response->body[$tab_index]->demonym,
              'area' => $response->body[$tab_index]->area,
              'gini' => $response->body[$tab_index]->gini,
              'nativeName' => $response->body[$tab_index]->nativeName,
              'subregion' => $response->body[$tab_index]->subregion,
              'alpha2Code' => $response->body[$tab_index]->alpha2Code,
              'alpha3Code' => $response->body[$tab_index]->alpha3Code,
              'currencies' => implode(",", $response->body[$tab_index]->currencies),
              'languages' => implode(",", $response->body[$tab_index]->languages),
              'callingCodes' => implode(",", $response->body[$tab_index]->callingCodes),
              'borders' => implode(",", $response->body[$tab_index]->borders),
              'timezones' => $timezones,
              'latlng' => implode(",", $response->body[$tab_index]->latlng),
              'altSpellings' => implode(",", $response->body[$tab_index]->altSpellings),
            );

            foreach ($response->body[$tab_index]->translations as $lang) {
                $translations = $lang;
                if (count($response->body[$tab_index]->translations) > 1) {
                    $translations .= ', ';
                }
            }

            $fields['translations'] = $translations;


            $country_cid = db_insert('country_list')
                ->fields($fields)
                ->execute();

            // Convert string to int.
            $country_cid = (int) $country_cid;
        }

        watchdog('country updates', 'Country ' . $response->body[$tab_index]->name . ' was updated at' . REQUEST_TIME);
    }
    else {
        drupal_set_message(t('@message', array('@message' => $response->body->message)), 'warning');
        drupal_set_message(t('@message', array('@message' => $response->headers[0])), 'warning');
    }
}

/**
 * Implements hook_help()
 * @return boolean
 */
function world_location_help($path, $arg) {
    
}

/**
 * Country fields to retrieve.
 * @param type $form
 * @param type $form_state
 */
function world_locations_form_api_fields($form, &$form_state) {

    // $country_array = array();

    $form['country_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Country field who want to retrieve'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $result = db_query('SELECT co.id, co.name
                        FROM {country_list} co '
    );

    $countries = $result->fetchAll();

    foreach ($countries as $country) {
        $form['country_fields'][$country->country] = array(
          '#type' => 'fieldset',
          '#title' => t($country->country . ' ' . 'fields'),
          '#weight' => 5,
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );

        $form['country_fields'][$country->country]['fields'] = array(
          '#type' => 'checkboxes',
          '#title' => t($country->subregion),
          '#options' =>
          array(
            'name_' . $country->id => t('name'),
            'capital_' . $country->id => t('capital'),
            'region_' . $country->id => t('region'),
            'subregion_' . $country->id => t('subregion'),
            'area_' . $country->id => t('area'),
            'timezones_' . $country->id => t('timezones'),
            'nativeName_' . $country->id => t('nativeName'),
            'callingCodes_' . $country->id => t('callingCodes'),
            'alpha2Code_' . $country->id => t('alpha2Code'),
            'currencies_' . $country->id => t('currencies'),
            'alpha3Code_' . $country->id => t('alpha3Code'),
            'languages_' . $country->id => t('languages'),
          ),
        );
    }
    $form['country_fields']['update_button'] = array(
      '#type' => 'submit',
      '#value' => t('Update field list'),
      '#weight' => -10,
        //   '#submit' => array('_update_countries_list'),
    );


    return $form;
}

function world_locations_form_blocks($form, &$form_state) {

    $form = array();

    $form['blocks'] = array(
      '#type' => 'fieldset',
      '#title' => t('Blocks'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['blocks']['total_info'] = array(
      '#type' => 'textfield', //you can find a list of available types in the form api
      '#title' => t('Total info blocks'),
      '#size' => 60,
      '#maxlength' => 60,
      '#default_value' => variable_get('total_info_blocks', 2),
    );

    $form['blocks']['save_button'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => -10,
        //   '#submit' => array('_update_countries_list'),
    );
    return $form;
}

function world_locations_form_blocks_submit($form, &$form_state) {

    if (is_numeric($form_state['values']['total_info']) && $form_state['values']['total_info'] > 0) {
        variable_set('total_info_blocks', $form_state['values']['total_info']);
    }
    else {
        drupal_set_message(t('Value should be a number'), 'warning');
    }
}

/*
  function world_locations_form_api_fields_submit($form, &$form_state) {

  foreach ($form_state['values']['fields'] as $field) {

  if(!empty($field)){
  $num_updated = db_update('country_fields') // Table name no longer needs {}
  ->fields(array(
  'active' => 1,
  ))
  ->condition('field_name', $field, '=')
  ->condition('id', $field, '=')
  ->execute();
  }

  drupal_set_message(t('@num fields have been updated', array('@num' => $num_updated)));
  }
  } */

function get_all_countries() {
    $result = db_query('SELECT co.name,co.region
                        FROM {country_list} co  '
    );

    $countries = $result->fetchAll();

    return $countries;
}

/**
 * Remove -ANY- option from exposed countries views.
 * @param type $form
 * @param type $form_state
 * @param type $form_id
 */
function world_locations_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
drupal_add_css(drupal_get_path('module', 'world_locations') . '/css/world_locations.css', 
                                array('group' => CSS_DEFAULT, 'every_page' => FALSE));
    switch ($form_id) {
        case 'views_exposed_form':     
            if (!empty($form['#id']) && ($form['#id'] == 'views-exposed-form-select-country-page' || 
                                         $form['#id'] == 'views-exposed-form-select-country-page-1')) {
                                         //array_push($form['region']['#attributes']['class'], 'checkbox');

                // Remove the start date label
                if (isset($form['region'])) {
                    unset($form['region']['#options']['All']);
                }
                
                break;
            }
    }
}
