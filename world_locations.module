<?php

/**
 * @file
 * The module retrieve countries data and information about it.
 * and create Nº blocks for show countries.
 * code uses mashape API.
 */
module_load_include('inc', 'world_locations', 'world_locations.admin');

function world_locations_menu() {

    $items['admin/config/services/countriesapi'] = array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array('world_locations_form_api_countries'),
      'title' => 'Country location settings',
      'description' => 'Set API credential for list countries',
      'access arguments' => array('administer site configuration'),
      'file' => 'world_locations.admin.inc',
  
    );

    $items['admin/config/services/countriesapi/main'] = array(
      'title' => 'Country location settings',
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
/*
    $items['admin/config/services/countriesapi/fields'] = array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array('world_locations_form_api_fields'),
      'title' => 'Countries fields ',
      'description' => 'Check all country fields to retrieve',
      'type' => MENU_LOCAL_TASK,
      'access arguments' => array('administer site configuration'),
      'file' => 'world_locations.admin.inc',
      'weight' => 1,
    ); */

    $items['admin/config/services/countriesapi/blocks-config'] = array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array('world_locations_form_blocks'),
      'title' => 'Blocks config',
      'description' => 'Blocks configuration',
      'type' => MENU_LOCAL_TASK,
      'access arguments' => array('administer site configuration'),
      'file' => 'world_locations.admin.inc',
      'weight' => 2,
    );

    return $items;
}

/**
 * Implements hook_block_info().
 */
function world_locations_block_info() {
    $blocks = array();

    for ($i = 0; $i < variable_get('num_country_blocks', 5); $i++) {
        $blocks['country_info' . $i] = array(
          'info' => t('Show country info block ' . $i),
        );
    }
    $blocks['country_random'] = array(
      'info' => t('Show a random country info'),
    );

    return $blocks;
}

/**
 * Implements hook_block_view().
 */
function world_locations_block_view($delta = '') {
    $block = array();
    $country = FALSE;

    $country_info = (strstr($delta, 'country_info')) ? $delta : 'country_random';

    switch ($delta) {

        case $country_info:
            $country = variable_get('default_country_' . $delta, 'Germany');

            if ($country_info == 'country_random') {
                $countries = get_countries_names();
                foreach ($countries as $country) {
                    $country_list[$country->name] = $country->name;
                }

                $country = array_rand($country_list);
            }
            $block['subject'] = t('Country');
            $block['content'] = world_locations_country_info($country, $country_info);

            break;
    }
    return $block;
}

function world_locations_country_info($country = FALSE, $delta = NULL) {

    // Update Conutries list.
    require_once 'vendor/mashape/unirest-php/src/Unirest.php';

    $app_key = variable_get('X-Mashape-Key', '');
    $codification = variable_get('encode', 'application/json');

    // These code snippets use an open-source library. http://unirest.io/php
    $response = Unirest\Request::get("https://restcountries-v1.p.mashape.com/name/" . $country, array(
          "X-Mashape-Key" => $app_key,
          "Accept" => $codification
            )
    );


    if ($response->code == 400) {
        $output = t('Imposible retrive data about <strong>' . $country . '</strong> , please try again later');
        return $output;
    }
    $output = '<ul class="countries-block">';
    $index = '';

    if (isset($response->body[0])) {
        foreach ($response->body[0] as $index => $value) {

            if (!is_array($value) && !is_object($value)) {
                $status_field = db_query('SELECT cb.' . $index . ' FROM {country_blocks} cb WHERE cb.id = :delta  AND cb.' . $index . ' = :status ', array(':delta' => $delta,
                  ':status' => 1)
                    )->fetchCol();

                if (!empty($status_field)) {
                    $output .= '<li>' . $index . ' : <strong>' . $value . '</strong></li>';
                    $actual_index = $index;
                }
            }
        }

        $output .= '</ul>';
    }

    return $output;
}

function world_locations_block_configure($delta = '') {
    // This example comes from node.module.
    $form = array();

    $delta = (strstr($delta, 'country_info')) ? $delta : 'country_random';

    if ($delta != 'country_random') {

        $countries = get_countries_names();
        foreach ($countries as $country) {
            $country_list[] = $country->name;
        }
        $form['select_country'] = array(
          '#type' => 'select',
          '#title' => t('Country to show'),
          '#default_value' => variable_get('default_country_' . $delta, 'Spain'),
          '#options' => drupal_map_assoc($country_list),
        );
    }

    $fields = array();

    $delta_fields = db_query('SELECT * FROM {country_blocks} WHERE id = :delta ', array(':delta' => $delta))->fetchAll();

    if (!empty($delta_fields[0])) {
        foreach ($delta_fields[0] as $index => $value) {
            if ($value >= 0 && $index != 'id') {
                $fields[$index] = $index;
                if ($value == 1) {
                    $default_values[$index] = $index;
                }
            }
        }
    }
    else {
        // First time when create the block and row not exists in database, we create it with all fields active by default.
        $default_values = array(
          'name' => 'name',
          'capital' => 'capital',
          'region' => 'region',
          'subregion' => 'subregion',
          'relevance' => 'relevance',
          'population' => 'population',
          'demonym' => 'demonym',
          'area' => 'area',
          'gini' => 'gini',
          'nativeName' => 'nativeName',
          'alpha2Code' => 'alpha2Code',
          'alpha3Code' => 'alpha3Code',
          'currencies' => 'currencies',
          'languages' => 'languages',
          'callingCodes' => 'callingCodes',
          'borders' => 'borders',
          'timezones' => 'timezones',
          'latlng' => 'latlng',
          'altSpellings' => 'altSpellings',
          'translations' => 'translations',
        );
    }

    $form['select_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Fields to output'),
      '#default_value' => $default_values,
      '#options' => (!empty($fields)) ? $fields : $default_values,
    );

    return $form;
}

/**
 * Implements hook_block_save().
 *
 * This hook declares how the configured options for a block
 * provided by this module are saved.
 */

/**
 * Implements hook_block_save().
 *
 * This hook declares how the configured options for a block
 * provided by this module are saved.
 */
function world_locations_block_save($delta = '', $edit = array()) {
    // We need to save settings from the configuration form.
    // We need to check $delta to make sure we are saving the right block.
    $delta = (strstr($delta, 'country_info')) ? $delta : 'country_random';

    if ($delta != 'country_random') {
        // Have Drupal save the string to the database.
        variable_set('default_country_' . $delta, $edit['select_country']);
    }


    if (!empty($edit['select_fields']) && is_array($edit['select_fields'])) {

        $exists_delta = db_query('SELECT 1 FROM {country_blocks} WHERE id = :delta', array(':delta' => $delta))->fetchField();

        // Insert the first time.
        if (!$exists_delta) {

            $country_cid = db_insert('country_blocks')
                ->fields(array('name' => 1,
                  'id' => $delta,
                  'capital' => 1,
                  'region' => 1,
                  'subregion' => 1,
                  'relevance' => 1,
                  'population' => 1,
                  'demonym' => 1,
                  'area' => 1,
                  'gini' => 1,
                  'nativeName' => 1,
                  'alpha2Code' => 1,
                  'alpha3Code' => 1,
                ))
                ->execute();

            // Convert string to int.
            $country_cid = (int) $country_cid;
        }
        else {

            $update_fields = array();
            foreach ($edit['select_fields'] as $index => $value) {
                if (!empty($value)) {
                    $update_fields[$index] = 1;
                }
                else {
                    $update_fields[$index] = 0;
                }
            }

            $update_delta = db_update('country_blocks')
                ->fields($update_fields)
                ->condition('id', $delta)
                ->execute();
        }
    }
}

/**
 * Get allc ountries names.
 */
function get_countries_names() {

    // Query all countries.
    $result = db_query('SELECT co.id, co.name, co.register_path,  co.search_path
                        FROM {country_list} co '
    );

    $countries = $result->fetchAll();

    return $countries;
}

/**
 * Implements hook_views_api().
 */
function world_locations_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'world_locations') . '/includes/views',
  );
}
/**
* Implements hook_libraries_info().
*/
function world_locations_libraries_info() {

  // A very simple library. No changing APIs (hence, no versions), no variants.
  // Expected to be extracted into 'sites/all/libraries/simple'.
  $libraries['mashape'] = array(
    'name' => 'Mashape library',
    'vendor url' => 'https://www.mashape.com/',
    'download url' => 'https://github.com/Mashape/unirest-php',
   /* 'version arguments' => array(
      'file' => 'readme.txt',
      // Best practice: Document the actual version strings for later reference.
      // 1.x: Version 1.0
      'pattern' => '/Version (\d+)/',
      'lines' => 5,
    ), */
  'version callback' => 'mashape_version_callback',
    'files' => array(
      'php' => array('Unirest.php'),
    ),
  );

  return $libraries;
}

function mashape_version_callback() {
  return TRUE;
}
